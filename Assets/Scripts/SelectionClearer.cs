﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SelectionClearer : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            SpriteManager.current.ClearSelectedSprite();
        }
    }
}
