﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;
using System.Linq;

public class SpriteManager : MonoBehaviour
{

    public static SpriteManager current;

    private Dictionary<SpriteRenderer, MySprite> rendererToSpriteMap = new Dictionary<SpriteRenderer, MySprite>();
    private Dictionary<MySprite, SpriteRenderer> spriteToRendererMap = new Dictionary<MySprite, SpriteRenderer>();

    private MySprite selectedSprite = null;
    public Shape selectedShape;
    private Color _color;
    public Color selectedColor
    {
        get { return _color; }
        set
        {
            _color = value;
            if (selectedSprite != null)
            {
                ChangeSpriteColor();
            }
        }
    }

    public SpriteRenderer spritePrefab;
    public Sprite diamondSprite;
    public Sprite triangleSprite;
    public Sprite hexSprite;

    public Transform selectionIndicator;

    void Awake()
    {
        current = this;
    }

    public void SpawnSprite(Vector3 position)
    {
        MySprite mySprite = new MySprite(selectedShape, position, selectedColor);
        var renderer = Instantiate(spritePrefab);

        Sprite sprite;
        int sides;
        switch (selectedShape)
        {
            case Shape.Diamond:
                sprite = diamondSprite;
                sides = 4;
                break;
            case Shape.Triangle:
                sprite = triangleSprite;
                sides = 3;
                break;
            case Shape.Hex:
                sprite = hexSprite;
                sides = 6;
                break;
            default:
                return;
        }

        renderer.sprite = sprite;
        renderer.color = selectedColor;
        renderer.transform.position = position;

        renderer.GetComponent<PolygonCollider2D>().CreatePrimitive(sides, Vector2.one * .5f);

        spriteToRendererMap.Add(mySprite, renderer);
        rendererToSpriteMap.Add(renderer, mySprite);
    }

    public void SetShapeByInt(int newShape)
    {
        selectedShape = (Shape)newShape;
    }

    public void ChangeSpriteColor()
    {
        selectedSprite.SetColor(selectedColor);
        spriteToRendererMap[selectedSprite].color = selectedColor;
    }

    public void ClearSelectedSprite()
    {
        selectedSprite = null;
        selectionIndicator.gameObject.SetActive(false);
    }

    public void SelectSpriteByRenderer(SpriteRenderer sr)
    {
        selectedSprite = rendererToSpriteMap[sr];

        selectionIndicator.gameObject.SetActive(true);
        selectionIndicator.position = sr.transform.position.WithZ(-.5f);

    }

    public void DeleteSelectedSprite()
    {
        var r = spriteToRendererMap[selectedSprite];
        rendererToSpriteMap.Remove(r);
        spriteToRendererMap.Remove(selectedSprite);

        Destroy(r.gameObject);

        ClearSelectedSprite();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Delete) && selectedSprite != null)
        {
            DeleteSelectedSprite();
        }
    }

    public void SaveSprites()
    {
        var root = new XElement("Sprites");

        foreach (var s in spriteToRendererMap.Keys)
        {
            var e = new XElement("Sprite",
                new XElement("Shape", s.shape.ToString()),
                new XElement("Color",
                    new XElement("R", s.color.r),
                    new XElement("G", s.color.g),
                    new XElement("B", s.color.b)
                ),
                new XElement("Position", 
                    new XElement("X", s.position.x),
                    new XElement("Y", s.position.y)
                )
            );
            root.Add(e);

            //var e2 = new XElement("Sprite");
            //var child = new XElement("Shape", s.shape.ToString());
            //e2.Add(child);

            //child = new XElement("Color");
            //var grandchild = new XElement("R", s.color.r);
            //child.Add(grandchild);
            //grandchild = new XElement("G", s.color.g);
            //child.Add(grandchild);
            //grandchild = new XElement("B", s.color.b);
            //child.Add(grandchild);
            //e2.Add(child);
            
        }

        var dir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
        dir = Path.Combine(dir, "Sprite Game");
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        var file = Path.Combine(dir, "savedata.xml");
        File.WriteAllText(file, root.ToString());
    }

    public void LoadSprites()
    {
        foreach (var r in rendererToSpriteMap.Keys.ToList())
        {
            SelectSpriteByRenderer(r);
            DeleteSelectedSprite();
        }

        var dir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
        var file = Path.Combine(dir, "Sprite Game/savedata.xml");

        if (!File.Exists(file))
        {
            Debug.LogError("Couldn't find save data to load.");
            return;
        }

        var doc = XDocument.Load(file);
        var root = doc.Root;

        foreach (var e in root.Elements("Sprite"))
        {
            var shape = (Shape)System.Enum.Parse(typeof(Shape), e.Element("Shape").Value);
            var color = new Color((float)e.Element("Color").Element("R"),
                                  (float)e.Element("Color").Element("G"),
                                  (float)e.Element("Color").Element("B"));
            var position = new Vector2((float)e.Element("Position").Element("X"),
                                       (float)e.Element("Position").Element("Y"));

            selectedColor = color;
            selectedShape = shape;
            SpawnSprite(position);
        }
    }
}
