﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SelectSprite : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        SpriteManager.current.SelectSpriteByRenderer(GetComponent<SpriteRenderer>());
    }
}
