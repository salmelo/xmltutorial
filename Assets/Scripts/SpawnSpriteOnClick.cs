﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SpawnSpriteOnClick : MonoBehaviour, IPointerClickHandler
{


    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            SpriteManager.current.SpawnSprite(eventData.pointerCurrentRaycast.worldPosition.WithZ(-1));
        }
    }
}
