﻿using UnityEngine;
using System.Collections;

public class MySprite
{
    public Shape shape { get; private set; }
    public Vector2 position { get; private set; }
    public Color color { get; private set; }

    public MySprite(Shape shape, Vector2 position, Color color)
    {
        this.shape = shape;
        this.position = position;
        this.color = color;
    }

    public void SetColor(Color color)
    {
        this.color = color;
    }

}

public enum Shape
{
    Diamond, Triangle, Hex
}