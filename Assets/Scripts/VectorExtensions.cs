﻿using UnityEngine;
using System.Collections;

public static class VectorExtensions
{
    public static Vector3 WithZ(this Vector3 vector, float z)
    {
        var v = new Vector3(vector.x, vector.y, z);
        return v;
    }
}
